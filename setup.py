from setuptools import setup

requirements = list()
with open('requirements.txt') as f:
    requirements = f.read().splitlines()

readme = str()
with open('README.md') as f:
    readme = f.read()

setup(name='LibreDomo Server',

      # PEP 440 -- Version Identification and Dependency Specification
      version='0.0.1',

      # Project description
      description='LibreDome Server for OpenShift',
      long_description=readme,

      # Author details
      author='Salman Mohammadi',
      author_email='mohammadi@openmailbox.org',

      # Project details
      url='https://framagit.org/libredomo/openshift',
      license="AGPL v3",

      # Project dependencies
      install_requires=requirements,
      )
