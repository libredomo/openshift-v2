# LibreDomo Server on OpenShift

You can easily install LibreDomo Server on [OpenShift.com](https://openshift.com)


## Install rhc on Debian (9 or 8), Ubuntu (16.04 or14.04)

```bash
$ sudo apt install rhc
```


## Setup OpenShift

```bash
$ rhc setup
```

## Create a New Domain
Run the following command to create a new domain. Use your preferred name instead of *\<Domain_Name\>* .

```bash
$ rhc domain create <Domain_Name>
```

## Create Python application 

Create a Python3 application by using the following command

```bash
rhc app-create libredomo python-3.3 --from-code https://framagit.org/libredomo/openshift.git
```

## Checkout the Website

Checkout the LibreDomo server at

```
https://libredomo-<Domain_Name>.rhcloud.com
```

You can check out a sample live installation on OpenShift [here](https://home-libredomo.rhcloud.com/).